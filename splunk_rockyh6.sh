#!/bin/bash

# Define Splunk version and download URL
SPLUNK_VERSION="8.2.2"  # Change this to the latest version or the specific version you need
SPLUNK_DOWNLOAD_URL="https://download.splunk.com/products/splunk/releases/${SPLUNK_VERSION}/linux/splunk-${SPLUNK_VERSION}-Linux-x86_64.tgz"

# Define installation directory
SPLUNK_DIR="/opt/splunk"

# Update system and install necessary packages
sudo dnf update -y
sudo dnf install wget -y

# Download Splunk
wget -O splunk.tgz "${SPLUNK_DOWNLOAD_URL}"

# Extract Splunk
sudo tar -xzvf splunk.tgz -C /opt

# Configure Splunk to start at boot
sudo $SPLUNK_DIR/bin/splunk enable boot-start --accept-license --answer-yes --no-prompt

# Start Splunk
sudo $SPLUNK_DIR/bin/splunk start

# Print end message
echo "Splunk installation and basic setup completed."
