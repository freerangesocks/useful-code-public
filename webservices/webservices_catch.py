from flask import Flask, request

app = Flask(__name__)

@app.route('/receive_message', methods=['GET'])
def receive_message():
    # Extract the message parameter from the URL
    message = request.args.get('message', '')

    # Print the message to console (or handle it as needed)
    print(f"Received message: {message}")

    # Respond to the beacon
    return "Message received successfully", 200

if __name__ == '__main__':
    # Run the server on all available interfaces, on port 5000
    app.run(host='0.0.0.0', port=5000)
