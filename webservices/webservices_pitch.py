import requests

def send_message(file_path, server_url):
    # Read the message from the file
    with open(file_path, 'r') as file:
        message = file.read().strip()

    # Send the message as part of a GET request
    response = requests.get(server_url, params={'message': message})
    print(f"Server response: {response.text}")

# Example usage
file_path = '/path/to/your/message.txt'
server_url = 'http://yourwebserver.com/receive_message'
send_message(file_path, server_url)
