import socket
import threading
import os
import time

def handle_client(client_socket):
    next_message_time = time.time() + 10  # Next time to send the message
    try:
        while True:
            # Send data from /dev/urandom
            data = os.urandom(1024)  # Read 1024 bytes
            client_socket.sendall(data)
            
            # Check if it's time to send the message
            if time.time() >= next_message_time:
                client_socket.sendall("81 Golden Red\n".encode())
                next_message_time = time.time() + 10  # Schedule next message
                
            time.sleep(1)  # Slow down the loop to make the output manageable
    except (BrokenPipeError, ConnectionResetError):
        # Handle cases where the client disconnects
        pass
    finally:
        client_socket.close()

def start_server(port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        server_socket.bind(('', port))
        server_socket.listen()

        print(f"Server listening on port {port}...")

        while True:
            client_socket, addr = server_socket.accept()
            print(f"Accepted connection from {addr}")
            client_thread = threading.Thread(target=handle_client, args=(client_socket,))
            client_thread.start()

if __name__ == "__main__":
    PORT = 2600
    start_server(PORT)
