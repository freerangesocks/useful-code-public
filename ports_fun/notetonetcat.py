import socket

def send_file_contents(client_socket, file_path):
    """Send the contents of a specified file to the client."""
    with open(file_path, 'r') as file:
        file_contents = file.read()

    message = "Here's the secret:\n" + file_contents
    client_socket.sendall(message.encode())

def start_server(port, file_path):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server_socket:
        server_socket.bind(('', port))
        server_socket.listen()

        print(f"Server listening on port {port}...")

        while True:
            client_socket, _ = server_socket.accept()
            with client_socket:
                print("Sending file contents to client...")
                send_file_contents(client_socket, file_path)

if __name__ == "__main__":
    PORT = 122
    FILE_PATH = '/path/to/safe-file'  # Replace with a path to a non-sensitive file.
    start_server(PORT, FILE_PATH)
