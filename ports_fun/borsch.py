import socket
import time

message = "воздушный шлюз открыт".encode('utf-8')
target_ips = ["192.168.1.%d" % i for i in range(1, 255)]  # Adjust the IP range
target_ports = [80, 443]  # Example ports, adjust as needed

def broadcast_message(interval):
    while True:
        for ip in target_ips:
            for port in target_ports:
                try:
                    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    sock.sendto(message, (ip, port))
                except Exception as e:
                    print(f"Could not send to {ip}:{port}, {e}")
                finally:
                    sock.close()
        time.sleep(interval)

# Example usage, adjust the timing as needed
#broadcast_message(60)  # Every 1 minute
#broadcast_message(300)  # Every 5 minutes
broadcast_message(1800)  # Every 30 minutes
