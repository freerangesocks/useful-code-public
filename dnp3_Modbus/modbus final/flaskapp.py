from flask import Flask, jsonify
from pymodbus.client.sync import ModbusTcpClient

app = Flask(__name__)

MODBUS_SERVER_IP = '192.168.1.100'  # Update this to the IP of Machine B
MODBUS_SERVER_PORT = 502

@app.route('/')
def home():
    return '''
    <h2>Modbus Data Viewer</h2>
    <div id="modbus-data">Connecting...</div>
    <script>
        setInterval(() => {
            fetch("/modbus-data")
                .then(response => response.json())
                .then(data => {
                    document.getElementById("modbus-data").textContent = JSON.stringify(data);
                })
                .catch(console.error);
        }, 2000); // Update every 2 seconds
    </script>
    '''

@app.route('/modbus-data')
def modbus_data():
    client = ModbusTcpClient(MODBUS_SERVER_IP, port=MODBUS_SERVER_PORT)
    if not client.connect():
        return jsonify({'error': 'Failed to connect to Modbus server'}), 500

    response = client.read_holding_registers(0, 10)
    client.close()

    if response.isError():
        return jsonify({'error': str(response)}), 500
    else:
        return jsonify({'registers': response.registers})

if __name__ == '__main__':
    app.run(debug=True)
