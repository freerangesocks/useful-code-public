#include <stdio.h>
#include <errno.h>
#include <modbus.h>

int main(void) {
    modbus_t *ctx;
    modbus_mapping_t *mb_mapping;
    int server_socket;

    ctx = modbus_new_tcp("0.0.0.0", 502);
    if (ctx == NULL) {
        fprintf(stderr, "Unable to allocate libmodbus context\n");
        return -1;
    }

    mb_mapping = modbus_mapping_new(0, 0, 10, 0); // Initialize with 10 holding registers
    if (mb_mapping == NULL) {
        fprintf(stderr, "Failed to create modbus mapping: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        return -1;
    }

    // Simulate initial data in holding registers
    for(int i = 0; i < 10; i++) {
        mb_mapping->tab_registers[i] = i + 1;  // Example data
    }

    server_socket = modbus_tcp_listen(ctx, 1);
    if (server_socket == -1) {
        fprintf(stderr, "Unable to create the server socket: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        modbus_mapping_free(mb_mapping);
        return -1;
    }

    modbus_tcp_accept(ctx, &server_socket);

    // Server loop: handle requests (omitted for brevity)
    
    printf("Closing the server.\n");
    modbus_close(ctx);
    modbus_free(ctx);
    modbus_mapping_free(mb_mapping);

    return 0;
}
