from flask import Flask, jsonify, render_template_string
from pymodbus.client.sync import ModbusTcpClient

app = Flask(__name__)

MODBUS_SERVER_IP = '192.168.1.100'  # Update this to the IP of Machine B
MODBUS_SERVER_PORT = 502

@app.route('/')
def home():
    return render_template_string('''
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Modbus Data Viewer</title>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    </head>
    <body>
        <h2>Modbus Data Viewer</h2>
        <canvas id="modbusChart" width="600" height="400"></canvas>
        <script>
            const ctx = document.getElementById('modbusChart').getContext('2d');
            const modbusChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: Array.from({length: 10}, (_, i) => i), // Register indices as labels
                    datasets: [{
                        label: 'Register Values',
                        backgroundColor: 'rgb(255, 99, 132)',
                        borderColor: 'rgb(255, 99, 132)',
                        data: [],
                    }]
                },
                options: {}
            });

            setInterval(() => {
                fetch("/modbus-data")
                    .then(response => response.json())
                    .then(data => {
                        modbusChart.data.datasets[0].data = data.registers;
                        modbusChart.update();

                        // Check for Easter egg condition - example: register 0 value is 12345
                        if (data.registers[0] == 12345) {
                            modbusChart.data.datasets[0].backgroundColor = 'red';
                            modbusChart.data.datasets[0].borderColor = 'red';
                        } else {
                            modbusChart.data.datasets[0].backgroundColor = 'rgb(255, 99, 132)';
                            modbusChart.data.datasets[0].borderColor = 'rgb(255, 99, 132)';
                        }
                    })
                    .catch(error => console.error('Error fetching Modbus data:', error));
            }, 2000); // Update every 2 seconds
        </script>
    </body>
    </html>
    ''')

@app.route('/modbus-data')
def modbus_data():
    client = ModbusTcpClient(MODBUS_SERVER_IP, port=MODBUS_SERVER_PORT)
    if not client.connect():
        return jsonify({'error': 'Failed to connect to Modbus server'}), 500

    response = client.read_holding_registers(0, 10)
    client.close()

    if response.isError():
        return jsonify({'error': str(response)}), 500
    else:
        return jsonify({'registers': [value for value in response.registers]})

if __name__ == '__main__':
    app.run(debug=True)

