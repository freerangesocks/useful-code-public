from flask import Flask, jsonify, render_template_string
from pymodbus.client.sync import ModbusTcpClient
from pymodbus.payload import BinaryPayloadBuilder, BinaryPayloadDecoder, Endian
import threading
import time

app = Flask(__name__)

MODBUS_SERVER_IP = '192.168.1.100'
MODBUS_SERVER_PORT = 502

def send_easter_egg_periodically():
    while True:
        time.sleep(300)  # Wait for 5 minutes before sending the next Easter egg
        send_easter_egg()  # Function to send Easter egg

def send_easter_egg():
    client = ModbusTcpClient(MODBUS_SERVER_IP, port=MODBUS_SERVER_PORT)
    if client.connect():
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        for char in "EASTER":  # ASCII for 'EASTER'
            builder.add_16bit_int(ord(char))
        payload = builder.to_registers()
        client.write_registers(0, payload)
        client.close()

@app.route('/')
def home():
    return render_template_string('''
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Modbus Data Viewer</title>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    </head>
    <body>
        <h2>Modbus Data Viewer</h2>
        <canvas id="modbusChart" width="600" height="400"></canvas>
        <script>
            const ctx = document.getElementById('modbusChart').getContext('2d');
            const modbusChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: Array.from({length: 10}, (_, i) => i),
                    datasets: [{
                        label: 'Register Values',
                        backgroundColor: 'green',
                        borderColor: 'green',
                        data: [],
                    }]
                },
                options: {}
            });

            setInterval(() => {
                fetch("/modbus-data")
                    .then(response => response.json())
                    .then(data => {
                        modbusChart.data.datasets[0].data = data.registers;
                        modbusChart.update();
                        if (data.registers[9] === 12345) {
                            modbusChart.data.datasets[0].backgroundColor = 'red';
                            modbusChart.data.datasets[0].borderColor = 'red';
                        } else {
                            modbusChart.data.datasets[0].backgroundColor = 'green';
                            modbusChart.data.datasets[0].borderColor = 'green';
                        }
                    })
                    .catch(error => console.error('Error fetching Modbus data:', error));
            }, 2000); // Update every 2 seconds
        </script>
    </body>
    </html>
    ''')

@app.route('/modbus-data')
def modbus_data():
    client = ModbusTcpClient(MODBUS_SERVER_IP, port=MODBUS_SERVER_PORT)
    if not client.connect():
        client.close()
        return jsonify({'error': 'Failed to connect to Modbus server'}), 500

    response = client.read_holding_registers(0, 10)
    client.close()

    if response.isError():
        return jsonify({'error': str(response)}), 500
    else:
        return jsonify({'registers': [value for value in response.registers]})

if __name__ == '__main__':
    # Start a background thread to send the Easter egg every 5 minutes
    threading.Thread(target=send_easter_egg_periodically, daemon=True).start()
    app.run(debug=True)
