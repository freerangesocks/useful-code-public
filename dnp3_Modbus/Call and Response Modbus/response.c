#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <modbus.h>

#define EASTER_EGG "EASTER"
#define EASTER_EGG_LENGTH 6 // The length of the Easter egg string

int main(void) {
    modbus_t *ctx = modbus_new_tcp("0.0.0.0", 502);
    uint16_t tab_registers[32] = {0}; // Allocate more registers for flexibility
    int server_socket, rc;

    modbus_mapping_t *mb_mapping = modbus_mapping_new(0, 0, 32, 0);
    if (mb_mapping == NULL) {
        fprintf(stderr, "Failed to allocate the mapping: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        return -1;
    }

    server_socket = modbus_tcp_listen(ctx, 1);
    if (server_socket == -1) {
        fprintf(stderr, "Unable to listen: %s\n", modbus_strerror(errno));
        modbus_mapping_free(mb_mapping);
        modbus_free(ctx);
        return -1;
    }

    modbus_tcp_accept(ctx, &server_socket);

    for (;;) {
        uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
        rc = modbus_receive(ctx, query);
        if (rc > 0) {
            // Check if the Easter egg has been written
            int easter_egg_found = 1;
            for (int i = 0; i < EASTER_EGG_LENGTH; i++) {
                if (mb_mapping->tab_registers[i] != EASTER_EGG[i]) {
                    easter_egg_found = 0;
                    break;
                }
            }
            if (easter_egg_found) {
                // Set register 9 to 12345 as an indicator of Easter egg detection
                mb_mapping->tab_registers[9] = 12345;
            } else {
                // Reset the indicator if the Easter egg pattern is not found
                mb_mapping->tab_registers[9] = 0;
            }

            modbus_reply(ctx, query, rc, mb_mapping);
        } else if (rc == -1) {
            break; // Connection closed or error
        }
    }

    printf("Quitting the loop.\n");
    modbus_mapping_free(mb_mapping);
    modbus_close(ctx);
    modbus_free(ctx);

    return 0;
}
