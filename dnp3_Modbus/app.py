from flask import Flask, jsonify, render_template
from pymodbus.client.sync import ModbusTcpClient
import os

app = Flask(__name__)

# Example Modbus server address; replace with your actual Modbus server IP
MODBUS_SERVERS = os.getenv('MODBUS_SERVERS', '192.168.1.100').split(',')
MODBUS_PORT = 502

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/modbus-data')
def modbus_data():
    # Initialize an empty list to hold data from all servers
    all_data = []
    for server_ip in MODBUS_SERVERS:
        client = ModbusTcpClient(server_ip, port=MODBUS_PORT)
        client.connect()
        result = client.read_holding_registers(address=1, count=10)
        client.close()
        
        if not result.isError():
            data = result.registers
        else:
            data = [0] * 10  # default to zero if there's an error
        
        all_data.append(data)
    
    # For simplicity, returning the data from all servers as a single list
    return jsonify(all_data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
