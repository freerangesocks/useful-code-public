from pymodbus.server import StartTcpServer
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.datastore import ModbusSequentialDataBlock as ModbusDataBlock
from pymodbus.payload import BinaryPayloadBuilder, Endian
import threading
import random
import time

def update_registers(context, interval=120):  # 2 minutes interval
    """
    Update registers with an encoded message and random values at a specified interval.
    """
    slave_id = 0x00
    address = 0x00  # Starting address of the registers
    message = "shutdown the system on Tuesday"
    
    while True:
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        
        # Encode the message into the registers
        for char in message:
            builder.add_16bit_int(ord(char))
        values = builder.to_registers()
        
        # Fill the rest of the registers with random data
        while len(values) < 10:  # Assuming 10 registers for simplicity
            values.append(random.randint(0, 65535))
        
        # Update the context with the new values
        context[slave_id].setValues(3, address, values)
        time.sleep(interval)

if __name__ == "__main__":
    # Initialize the datastore for each type of register
    store = ModbusDataBlock(0x00, [0]*10)  # Initialize 10 registers with zeros
    context = ModbusSlaveContext(
        di=ModbusDataBlock.create(),
        co=ModbusDataBlock.create(),
        hr=store,
        ir=ModbusDataBlock.create()
    )
    context = ModbusServerContext(slaves=context, single=True)
    
    # Start the thread that will update the registers
    update_thread = threading.Thread(target=update_registers, args=(context,))
    update_thread.daemon = True
    update_thread.start()
    
    # Run the server
    StartTcpServer(context, address=("0.0.0.0", 502))
