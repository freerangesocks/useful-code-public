Compiling and Running the Java Solution
Install Java JDK: Ensure you have Java Development Kit (JDK) installed on your Linux machine. You can install it via your distribution's package manager. 

For example, on Ubuntu-based distributions, you can install OpenJDK with:


sudo apt update
sudo apt install default-jdk
Add j2mod to Your Project:

If you're using Maven, the dependency was already shown. Make sure your pom.xml is set up correctly, and Maven will handle the dependency.
Without Maven, you'll need to download the j2mod jar from its GitHub releases page or a central Maven repository and include it in your classpath.
Compile Your Java Program:
Assuming your Java file is named ModbusEasterEggServer.java and you've downloaded j2mod-x.x.x.jar to the same directory, you can compile it with:


javac -cp .:j2mod-x.x.x.jar ModbusEasterEggServer.java
Replace x.x.x with the actual version number of the j2mod jar file.

Run Your Java Program:
After compilation, run your program with:


java -cp .:j2mod-x.x.x.jar ModbusEasterEggServer
Compiling and Running the C Solution
Install libmodbus:
First, ensure libmodbus is installed on your Linux machine. On Ubuntu-based distributions, you can install it with:


sudo apt update
sudo apt install libmodbus-dev
Compile Your C Program:
Assuming your C file is named modbus_server.c, you can compile it using gcc or clang, linking against libmodbus. With gcc, the command would be:


gcc modbus_server.c -o modbus_server -lmodbus
This command compiles your C source code into an executable named modbus_server, linking it with the libmodbus library.

Run Your C Program:
After compiling, you can run your Modbus server with:


./modbus_server

Additional Steps ###############################################################################################################################

To compile and run the C solution with libmodbus on Rocky Linux 9, which is based on Red Hat Enterprise Linux and uses dnf or yum as its package manager, you'll follow a process similar to what's described for Ubuntu but with commands and packages suited for Rocky Linux.

Step 1: Install Development Tools and libmodbus
Install Development Tools: This includes gcc, make, and other essential development tools.


sudo dnf groupinstall "Development Tools"
Install libmodbus:

Rocky Linux might not have libmodbus directly in its default repositories. You can either download and compile libmodbus from source or enable an EPEL (Extra Packages for Enterprise Linux) repository where libmodbus might be available.
To enable the EPEL repository and install libmodbus, run:

sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
sudo dnf update
sudo dnf install libmodbus-devel
Step 2: Compile Your C Program
With libmodbus installed, you can compile your program modbus_server.c. Ensure your C source file is ready.

gcc modbus_server.c -o modbus_server -lmodbus
This command compiles your C source code into an executable named modbus_server, linking it with the libmodbus library.
Step 3: Run Your C Program
After compiling, you can run your Modbus server executable by:

./modbus_server
Notes on Installing libmodbus from Source
If you prefer to or need to install libmodbus from source because the version in EPEL doesn't meet your needs, or if you'd rather not use EPEL, follow these steps:

Download the latest source from the libmodbus GitHub repository (https://github.com/stephane/libmodbus).

Extract the source code and navigate to the extracted directory.

Compile and install:


./autogen.sh  # If you cloned the git repo or if the release version requires
./configure
make
sudo make install
Link against the installed library:

After installing from source, you might need to tell your linker where to find the libmodbus library.
This often involves setting the LD_LIBRARY_PATH environment variable or adjusting the /etc/ld.so.conf file and running ldconfig.
Compiling and running on Rocky Linux 9 requires these additional steps primarily due to differences in package management and available repositories compared to Ubuntu-based distributions.
