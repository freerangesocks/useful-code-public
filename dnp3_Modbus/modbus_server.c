#include <stdio.h>
#include <errno.h>
#include <modbus/modbus.h>

int main(void) {
    modbus_t *ctx;
    modbus_mapping_t *mb_mapping;
    int server_socket;

    ctx = modbus_new_tcp("0.0.0.0", 502);
    if (ctx == NULL) {
        fprintf(stderr, "Unable to allocate libmodbus context\n");
        return -1;
    }

    mb_mapping = modbus_mapping_new(0, 0, 10, 0); // Create mapping with 10 holding registers
    if (mb_mapping == NULL) {
        fprintf(stderr, "Failed to create modbus mapping: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        return -1;
    }

    server_socket = modbus_tcp_listen(ctx, 1); // Listen for 1 connection
    if (server_socket == -1) {
        fprintf(stderr, "Unable to listen TCP connection: %s\n", modbus_strerror(errno));
        modbus_free(ctx);
        modbus_mapping_free(mb_mapping);
        return -1;
    }

    modbus_tcp_accept(ctx, &server_socket);

    // Server loop: handle requests
    for (;;) {
        uint8_t query[MODBUS_TCP_MAX_ADU_LENGTH];
        int rc = modbus_receive(ctx, query);
        if (rc > 0) {
            modbus_reply(ctx, query, rc, mb_mapping);
        } else if (rc == -1) {
            // Connection closed or error
            break;
        }
    }

    printf("Closing the server.\n");
    close(server_socket);
    modbus_free(ctx);
    modbus_mapping_free(mb_mapping);

    return 0;
}
