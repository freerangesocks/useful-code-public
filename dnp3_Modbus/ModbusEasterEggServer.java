import com.ghgande.j2mod.modbus.facade.ModbusTCPSlave;
import com.ghgande.j2mod.modbus.procimg.SimpleRegister;
import com.ghgande.j2mod.modbus.procimg.SimpleInputRegister;

public class ModbusEasterEggServer {

    public static void main(String[] args) {
        try {
            ModbusTCPSlave slave = new ModbusTCPSlave("0.0.0.0", 502, 1);
            slave.open();
            
            // Initialize registers
            for (int i = 0; i < 10; i++) {
                slave.addRegister(new SimpleRegister(0));
            }

            String message = "shutdown the system on Tuesday";
            while (true) {
                // Encode message in registers
                for (int i = 0; i < message.length() && i < 10; i++) {
                    slave.getProcessImage(1).setRegister(i, new SimpleRegister(message.charAt(i)));
                }
                
                // Fill the rest with random data
                for (int i = message.length(); i < 10; i++) {
                    slave.getProcessImage(1).setRegister(i, new SimpleRegister((int) (Math.random() * 65535)));
                }

                Thread.sleep(120000); // Update every 2 minutes
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
