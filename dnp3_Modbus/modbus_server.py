from pymodbus.server.sync import StartTcpServer
from pymodbus.datastore import ModbusSequentialDataStore, ModbusSlaveContext, ModbusServerContext
from pymodbus.payload import BinaryPayloadBuilder, Endian
import threading
import random
import time

def update_registers(context, interval=120):  # 2 minutes interval
    slave_id = 0x00
    address = 0x00
    message = "shutdown the system on Tuesday"
    
    while True:
        builder = BinaryPayloadBuilder(byteorder=Endian.Big, wordorder=Endian.Big)
        
        # Encode message for every 2 minutes
        for char in message:
            builder.add_16bit_int(ord(char))
        values = builder.to_registers()
        
        # Fill the rest of the registers with random data
        while len(values) < 10:  # Assuming 10 registers for simplicity
            values.append(random.randint(0, 65535))
        
        context[slave_id].setValues(3, address, values)
        time.sleep(interval)

if __name__ == "__main__":
    store = ModbusSequentialDataStore(0)
    context = ModbusSlaveContext(di=store, co=store, hr=store, ir=store)
    context = ModbusServerContext(slaves=context, single=True)
    
    update_thread = threading.Thread(target=update_registers, args=(context,))
    update_thread.daemon = True
    update_thread.start()
    
    StartTcpServer(context, address=("0.0.0.0", 502))
