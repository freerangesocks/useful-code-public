Machine A: Flask Server and Modbus Client
  create a /templates directory and place :
    app.py 
    index.html

Machine B: Modbus Server with Easter Egg
    place modbus_server.py


Here is the directory structure that is necessary for the proggy to work

Machine A ( Flask Server and Modbus Client )

FlaskApp/
│
├── app.py              # The Flask application script.
│
├── templates/          # Directory for Flask templates.
│   └── index.html      # The HTML template for the main page.
│
└── venv/               # Python virtual environment (optional but recommended).

Machine B ( Modbus Server )

ModbusServer/
│
├── modbus_server.py     # The Modbus server script.
│
└── venv/                # Python virtual environment (optional but recommended).

additional info: 

On both machines, ensure you have Python and pip installed.

Install Flask and pymodbus globally:

On Machine A, install Flask and pymodbus by running:

pip install Flask pymodbus

On Machine B, since you only need pymodbus for the Modbus server, run:

pip install pymodbus

Run the scripts:

On Machine A, navigate to the directory containing app.py and run:

python app.py

On Machine B, navigate to the directory containing modbus_server.py and run:

python modbus_server.py
Repeat the installation and running steps for any additional Machine B instances.

