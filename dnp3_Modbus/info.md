Running the Scripts and Setting Up the Environment
Machine A (Flask Server):

Install Flask and pymodbus (pip install Flask pymodbus).
Run app.py using python app.py.
The Flask application will be accessible at http://<Machine-A-IP>:5000.
You can specify multiple Modbus server IPs by setting the MODBUS_SERVERS environment variable before running app.py, for example:

export MODBUS_SERVERS=192.168.1.100,192.168.1.101
Machine B (Modbus Server):

Install pymodbus (pip install pymodbus).
Run modbus_server.py using python modbus_server.py.
Repeat the setup for any additional "Machine B" type devices as needed.

Easter Egg Decoding
The Easter egg message "shutdown the system on Tuesday" is encoded in the registers as ASCII values.
