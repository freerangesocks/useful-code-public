from pymodbus.server import StartTcpServer
from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.payload import BinaryPayloadBuilder
from pymodbus.constants import Endian
import threading
import random
import time

def update_registers(context, interval=120):  # Update every 2 minutes
    slave_id = 0x00
    address = 0x00
    message = "shutdown the system on Tuesday"
    
    while True:
        builder = BinaryPayloadBuilder(byteorder=Endian.Big)
        
        # Encode the message as 16-bit integers (ASCII values of each character)
        for char in message:
            builder.add_16bit_int(ord(char))
        encoded_message = builder.to_registers()
        
        # Fill the rest with random data to maintain 10 registers
        while len(encoded_message) < 10:
            encoded_message.append(random.randint(0, 65535))
        
        # Write the encoded message and random data to the context
        context[slave_id].setValues(3, address, encoded_message)
        time.sleep(interval)

if __name__ == "__main__":
    block = ModbusSequentialDataBlock(0, [0]*10)  # Initialize 10 registers
    store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block)
    context = ModbusServerContext(slaves=store, single=True)
    
    update_thread = threading.Thread(target=update_registers, args=(context,))
    update_thread.daemon = True
    update_thread.start()
    
    print("Starting Modbus server...")
    # Adjusting server start-up to match expected function signature
    server = StartTcpServer(context=context, address=("0.0.0.0", 502))
