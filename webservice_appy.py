from flask import Flask, jsonify, render_template
from pymodbus.client.sync import ModbusTcpClient

app = Flask(__name__)

# Replace 'localhost' and '502' with your Modbus server address and port
MODBUS_SERVER = 'localhost'
MODBUS_PORT = 502

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/modbus-data')
def modbus_data():
    client = ModbusTcpClient(MODBUS_SERVER, port=MODBUS_PORT)
    client.connect()
    # Example: Read a specific holding register; adjust address and count as needed
    result = client.read_holding_registers(address=1, count=1)
    client.close()
    
    if not result.isError():
        # Assuming the register holds an integer
        data = result.registers[0]
    else:
        data = 0 # or handle error accordingly
    
    return jsonify(value=data)

if __name__ == '__main__':
    app.run(debug=True)
