#!/bin/bash

# Prompt for Moodle site URL
read -p "Enter your Moodle site URL (e.g., example.com): " MOODLE_URL

# Prompt for Moodle database details
read -p "Enter your Moodle database name: " DB_NAME
read -p "Enter your Moodle database username: " DB_USER
read -sp "Enter your Moodle database password: " DB_PASSWORD
echo

# Update system packages
sudo dnf update -y

# Install Apache web server
sudo dnf install httpd -y

# Install MariaDB database server
sudo dnf install mariadb-server -y

# Start and enable Apache and MariaDB services
sudo systemctl start httpd mariadb
sudo systemctl enable httpd mariadb

# Secure MariaDB installation
sudo mysql_secure_installation <<EOF

y
$DB_PASSWORD
$DB_PASSWORD
y
y
y
y
EOF

# Create Moodle database and user
sudo mysql -uroot -p$DB_PASSWORD <<MYSQL_SCRIPT
CREATE DATABASE $DB_NAME CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER '$DB_USER'@'localhost' IDENTIFIED BY '$DB_PASSWORD';
GRANT ALL PRIVILEGES ON $DB_NAME.* TO '$DB_USER'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT

# Install PHP and required extensions
sudo dnf install php php-cli php-common php-mysqlnd php-xml php-mbstring php-gd php-curl php-zip php-ldap -y

# Configure PHP settings
sudo sed -i 's/memory_limit = .*/memory_limit = 512M/' /etc/php.ini

# Clone Moodle from Git repository
sudo dnf install git -y
git clone git://github.com/moodle/moodle.git /tmp/moodle

# Copy Moodle codebase to web root directory
sudo cp -r /tmp/moodle /var/www/html/moodle

# Set permissions
sudo chown -R apache:apache /var/www/html/moodle
sudo chmod -R 755 /var/www/html/moodle

# Setup Moodle cron job
(crontab -l ; echo "* * * * * /usr/bin/php /var/www/html/moodle/admin/cli/cron.php") | crontab -

# Provide confirmation
echo "Moodle installation and setup completed successfully!"
echo "Your Moodle site URL: http://$MOODLE_URL/moodle"
